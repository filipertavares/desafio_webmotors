#language: pt
@webmotors
Funcionalidade: Pesquisar carro
	Como um comprador
	Eu quero realizar uma pesquisa de um carro no site webmotors 
	Para testar a funcionalidade de pesquisa do site
	
Cenário: buscar carro
	Dado que acesso o site webmotors
	Quando pesquiso um carro da marca honda e modelo city
	Então o site me retorna uma lista de carros
	E saio do browser
	

	
	