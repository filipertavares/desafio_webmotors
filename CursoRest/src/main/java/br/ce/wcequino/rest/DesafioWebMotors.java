package br.ce.tavares.steps;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;

public class DesafioWebMotors {
	
	private WebDriver driver;
	
	@Dado("que acesso o site webmotors")
	public void queAcessoOSiteWebmotors() {
		System.setProperty("webdriver.chrome.driver", "C:\\eclipse\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.webmotors.com.br/carros/estoque?tipoveiculo=carros&estadocidade=estoque");
	}

	@Quando("pesquiso um carro da marca honda e modelo city")
	public void pesquisoUmCarroDaMarcaHondaEModeloCity() {
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"root\"]/main/div[1]/div[2]/div/div[1]/div[2]/div[2]/div/form/div[3]/div[2]/div/div/a[4]/small"))).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"root\"]/main/div[1]/div[2]/div/div[1]/div[2]/div[2]/div/form/div[3]/div[2]/div[2]/div[2]"))).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"root\"]/main/div[1]/div[2]/div/div[3]/div/div[5]/a[2]"))).click();		
	}

	@Então("o site me retorna uma lista de carros")
	public void oSiteMeRetornaUmaListaDeCarros() {
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.tagName("h1")));
		//String busca = driver.findElement(By.tagName("h1")).getAttribute("innerText");
		JavascriptExecutor js = (JavascriptExecutor)driver;
		String busca = ((JavascriptExecutor) driver).executeScript("return arguments[0].innerText;",driver.findElement(By.tagName("h1"))).toString();
		Assert.assertEquals("Honda City Novos e Usados", busca);
	}

	@Então("saio do browser")
	public void saioDoBrowser() {
		driver.quit();
	}



}
