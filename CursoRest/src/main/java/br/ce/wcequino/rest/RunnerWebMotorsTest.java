package br.ce.tavares.runners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;

@RunWith(Cucumber.class)
@CucumberOptions(
		glue = {"br.ce.tavares.config","br.ce.tavares.steps"},
		features = "src/test/resources/features/desafio_webmotors.feature",
		tags = {"@webmotors"},
		monochrome = true,
		snippets = SnippetType.CAMELCASE,
		dryRun = false,
		strict = false
		)

public class RunnerWebMotorsTest {

}
